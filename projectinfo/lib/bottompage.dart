import 'package:flutter/material.dart';
import 'package:projectinfo/homepage.dart';

class Bottombar extends StatefulWidget {
  @override
  _BottombarState createState() => _BottombarState();
}

final tabs = [HomePage(), HomePage(), HomePage(), HomePage()];
int currentindex = 0;

class _BottombarState extends State<Bottombar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        onPressed: () {},
        child: Icon(
          Icons.shopping_bag,
          color: Colors.black,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: tabs[currentindex],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              print(index);
              currentindex = index;
            });
          },
          currentIndex: currentindex,
          selectedItemColor: Colors.black,
          backgroundColor: Colors.white,
          items: [
            bottomButton(Icon(
              Icons.home,
            )),
            bottomButton(Icon(
              Icons.search,
            )),
            bottomButton(Icon(
              Icons.settings,
            )),
            bottomButton(Icon(
              Icons.person,
            )),
          ]),
    );
  }

  bottomButton(Icon icon) {
    return BottomNavigationBarItem(
      icon: icon,
      title: Text(''),
    );
  }
}
