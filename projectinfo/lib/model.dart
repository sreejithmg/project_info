class ChairModel {
  var name;
  var image;
  var price;
  var rating;
  ChairModel({this.name, this.image, this.price, this.rating});
  Map<String, dynamic> tomap() {
    return {'NAME': name, 'IMAGE': image, 'PRICE': price, 'RATING': rating};
  }
}
