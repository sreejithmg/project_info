import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:projectinfo/model.dart';
import 'package:sizer/sizer.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePAgeState();
  }
}

class HomePAgeState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  List chairList = [];
  var backgroundColor = Colors.grey[200];
  var textcolor;

  @override
  void initState() {
    addList();
    super.initState();
  }

  addList() {
    chairList.add(ChairModel(
        name: 'Tortor Chair',
        image: 'assets/chair1.jpg',
        price: '256.00',
        rating: '4.5'));
    chairList.add(ChairModel(
        name: 'Morbi Chair',
        image: 'assets/chair2.jpg',
        price: '284.00',
        rating: '4.8'));
    chairList.add(ChairModel(
        name: 'Pretium Chair',
        image: 'assets/chair3.jpg',
        price: '232.00',
        rating: '4.3'));
    chairList.add(ChairModel(
        name: 'Blandit Chair',
        image: 'assets/chair4.jpg',
        price: '224.00',
        rating: '4.1'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          sectionOne(),
          Padding(padding: EdgeInsets.all(15)),
          sectionTwo(),
          Padding(padding: EdgeInsets.all(15)),
          sectionThree()
        ],
      ),
    );
  }

  Widget sectionOne() {
    return Container(
      child: Row(
        children: [
          Text(
            'Top Rated',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
          Padding(padding: EdgeInsets.only(left: 30.h)),
          Icon(
            Icons.art_track_rounded,
            color: Colors.black,
          )
        ],
      ),
      margin: EdgeInsets.only(top: 10.h),
      height: 10.h,
      width: double.infinity,
      color: Colors.white,
    );
  }

  Widget sectionTwo() {
    return GestureDetector(
      onTap: () {
        setState(() {});
      },
      child: Row(children: [
        Padding(padding: EdgeInsets.only(left: 1.h)),
        returnChip(label: ' Arm Chair ', icon: Icon(Icons.chair)),
        Padding(padding: EdgeInsets.only(left: 2.h)),
        returnChip(label: ' Bed ', icon: Icon(Icons.bed)),
        Padding(padding: EdgeInsets.only(left: 2.h)),
        returnChip(label: ' LAmp ', icon: Icon(Icons.light)),
      ]),
    );
  }

  Widget sectionThree() {
    return Container(
        child: Expanded(
      child: GestureDetector(
          onTap: () {},
          child: Stack(children: [
            Card(
                child: Container(
              color: Colors.grey[200],
              height: 100.h,
              width: 100.h,
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                itemCount: chairList.length,
                itemBuilder: (BuildContext context, int index) => new Container(
                  child: Stack(
                    children: [
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: Row(
                            children: [
                              Text(
                                (chairList[index] as ChairModel).name,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          )),
                    ],
                  ),
                  height: 30.h,
                  width: 30.h,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                      image: AssetImage((chairList[index] as ChairModel).image),
                      fit: BoxFit.scaleDown,
                    ),
                    shape: BoxShape.rectangle,
                  ),
                ),
                staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count(2, index.isEven ? 3 : 2),
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
              ),
            ))
          ])),
    ));
  }

  returnChip({var label, var icon}) {
    return Chip(
        elevation: 12,
        backgroundColor: backgroundColor,
        padding: EdgeInsets.all(12),
        label: Text(label),
        avatar: icon,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))));
  }
}
